#!/bin/sh
set -eu

curl https://mvapich.cse.ohio-state.edu/download/mvapich/mv2x/mvapich2-x-ifs10.9-gnu4.8.5-omnipath-2.3rc2-2.el7.tgz |
tar xzf - --strip-components=1 mvapich2-x-ifs10.9-gnu4.8.5-omnipath-2.3rc2-2.el7/mvapich2-x-basic-ifs10.9-gnu4.8.5-omnipath-slurm-2.3rc2-2.el7.x86_64.rpm

curl https://mvapich.cse.ohio-state.edu/download/mvapich/mv2x/mvapich2-x-mofed4.2-gnu4.8.5-2.3rc2-1.el7.tgz |
tar xzf - --strip-components=1 mvapich2-x-mofed4.2-gnu4.8.5-2.3rc2-1.el7/mvapich2-x-basic-mofed4.2-gnu4.8.5-slurm-2.3rc2-1.el7.x86_64.rpm