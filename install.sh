#!/bin/sh
set -ex

PREFIX=/usr/local/software/mvapich2-X/2.3/opa
rpm --prefix=$PREFIX --dbpath=$PREFIX/.rpmdb --nodeps --test -Uvh mvapich2-x-basic-ifs10.9-gnu4.8.5-omnipath-slurm-2.3rc2-2.el7.x86_64.rpm &&
rpm --prefix=$PREFIX --dbpath=$PREFIX/.rpmdb --nodeps --noscripts -Uvh mvapich2-x-basic-ifs10.9-gnu4.8.5-omnipath-slurm-2.3rc2-2.el7.x86_64.rpm

PREFIX=/usr/local/software/mvapich2-X/2.3/mofed
rpm --prefix=$PREFIX --dbpath=$PREFIX/.rpmdb --nodeps --test -Uvh mvapich2-x-basic-mofed4.2-gnu4.8.5-slurm-2.3rc2-1.el7.x86_64.rpm &&
rpm --prefix=$PREFIX --dbpath=$PREFIX/.rpmdb --nodeps --noscripts -Uvh mvapich2-x-basic-mofed4.2-gnu4.8.5-slurm-2.3rc2-1.el7.x86_64.rpm

echo done