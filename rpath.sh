module load patchelf-0.9-gcc-5.4.0-v25fnm7

for lib in /usr/local/software/mvapich2-X/2.3/{opa,mofed}/lib64
do
    for f in $lib/libmpi{,cxx,fort}.so
    do
        patchelf --set-rpath /usr/local/software/slurm/current/lib $f
    done
done